package com.example.myblog.repository;

import com.example.myblog.entity.BlogEssay;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @auther : Dewey
 * @date : 2018/9/14 10 33
 * @description :
 */


public interface BlogEssayRepository extends JpaRepository<BlogEssay , Long> {

}
